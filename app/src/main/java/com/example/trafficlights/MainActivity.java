package com.example.trafficlights;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button redBtn ,yellowBtn, greenBtn;
    ConstraintLayout constraintLayout;

    int color;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        redBtn = (Button) findViewById(R.id.redButton);
        yellowBtn = (Button) findViewById(R.id.yellowButton);
        greenBtn = (Button) findViewById(R.id.greenButton);

        redBtn.setOnClickListener(this);
        yellowBtn.setOnClickListener(this);
        greenBtn.setOnClickListener(this);

        constraintLayout = (ConstraintLayout) findViewById(R.id.constaraintLayout);

    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt("color", color);
    }

    protected void onRestoreInstanceState(Bundle savedInastanceState) {
        super.onRestoreInstanceState(savedInastanceState);

        color = savedInastanceState.getInt("color");
        constraintLayout.setBackgroundColor(color);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.redButton:
                color = Color.RED;
                constraintLayout.setBackgroundColor(color);
                break;

            case R.id.yellowButton:
                color = Color.YELLOW;
                constraintLayout.setBackgroundColor(color);
                break;

            case R.id.greenButton:
                color = Color.GREEN;
                constraintLayout.setBackgroundColor(color);
                break;
        }
    }
}
